if ($('.search--expo-lg').length && $(window).width() > 991) {
  $(window).scroll(function() {
    if($(document).scrollTop() > 175) {
      $('.search--expo-sm').addClass('active');
      $('.form--search-expo').addClass('active');
    }
    else {
      $('.search--expo-sm').removeClass('active');
      $('.form--search-expo').removeClass('active');
    }
  })
}
