var $status = $('.presentation__counter-content');
var $presentation = $('.presentation__items-wrapper');

$presentation.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
    //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
    var i = (currentSlide ? currentSlide : 0) + 1;
  $('.presentation__counter-number--current').text(i);
  $('.presentation__counter-number--overall').text(slick.slideCount);
  $('.presentation__zoom-btn--big').attr('href', $('.slick-active').attr('href'));
  pdfScale = 1;
  $('.presentation__item img').css('transform', 'scale(1)');
});

$presentation.slick({
  prevArrow: $('.presentation__arrow--left'),
  nextArrow: $('.presentation__arrow--right')
});

var pdfScale = 1;

$('.presentation__zoom-btn--out').click(function(e) {
  e.preventDefault();
  pdfScale -= .1;
  $('.presentation__item.slick-active img').css('transform', 'scale(' + pdfScale + ')');
});

$('.presentation__zoom-btn--in').click(function(e) {
  e.preventDefault();
  pdfScale += .1;
  $('.presentation__item.slick-active img').css('transform', 'scale(' + pdfScale + ')');
});


$('[data-fancybox]').fancybox({
  buttons: [
    "zoom",
    //"share",
    "slideShow",
    //"fullScreen",
    //"download",
    "thumbs",
    "close"
  ],
});