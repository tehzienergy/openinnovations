$('.track__slider').slick({
  slidesToShow: 3,
  responsive: [
    {
      breakpoint: 1350,
      settings: {
        arrows: false,
        dots: true
      }
    },
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        arrows: false,
        dots: true
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        arrows: false,
        dots: true
      }
    }
  ]
})