$('.speakers-slider__content').on('init', function() {
  $('.speakers-slider .slick-active').prev().addClass('speakers-slider__item--prev');
})

$('.speakers-slider__content').slick({
  centerMode: true,
  variableWidth: true
}).on('beforeChange', function(event, slick, currentSlide, nextSlide) {
  if (Math.abs(nextSlide - currentSlide) == 1) {
    direction = (nextSlide - currentSlide > 0) ? "right" : "left";
  }
  else {
    direction = (nextSlide - currentSlide > 0) ? "left" : "right";
  }
  
  if (direction == 'left') {
    $('.speakers-slider .slick-slide').removeClass('speakers-slider__item--prev');
    $('.speakers-slider .slick-slide.slick-active').prev().prev().addClass('speakers-slider__item--prev');
  }
  
  else if (direction == 'right') {
    $('.speakers-slider .slick-slide').removeClass('speakers-slider__item--prev');
    $('.speakers-slider .slick-slide.slick-active').addClass('speakers-slider__item--prev');
  }
});
